import 'package:flutter/material.dart';
import 'package:streamers/stream/message_stream.dart';

import 'image_screen.dart';
import 'message_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return HomeScreenState();
  }
}

class HomeScreenState extends State<StatefulWidget>{
  late MessageStream messageStream = MessageStream();
  List<String> messages = [];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Streamer',
      home: Builder(
        builder: (context) => Scaffold(
          appBar: AppBar(title: const Text('Streamer Emulator')),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 15)),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(
                          builder: (context) => const MessageScreen(),
                        )
                    );
                  },
                  child: Container(
                    height: 50,
                    width: 200,
                    alignment: Alignment.center,
                    child: const Text('Send messages'),
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.all(10)
              ),
              Center(
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 15)),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(
                          builder: (context) => const ImageScreen(),
                        )
                    );
                  },
                  child: Container(
                    height: 50,
                    width: 200,
                    alignment: Alignment.center,
                    child: const Text('Send images'),
                  ),
                ),
              ),
            ],
          )
        ),
      )
    );
  }

}