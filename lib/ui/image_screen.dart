import 'package:flutter/material.dart';
import 'package:streamers/stream/image_stream.dart';
import 'package:streamers/ui/app.dart';

class ImageScreen extends StatefulWidget {
  const ImageScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ImageScreenState();
  }
}

class ImageScreenState extends State<StatefulWidget>{
  late ImagesStream imageStream = ImagesStream();
  List<String> images = [];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Streamer',
      home: Scaffold(
        appBar: AppBar(
            title: const Text('Streamer Images'),
            leading: IconButton(
                icon: const Icon(Icons.arrow_back),
                onPressed: () {
                  Navigator.push(context,
                    MaterialPageRoute(
                        builder: (context) => const HomeScreen()),
                  );
                }
            )
        ),
        body: ListView.builder(
            padding: const EdgeInsets.all(8),
            itemCount: images.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(images[index]),
                    fit: BoxFit.fitWidth,
                  )
                ),
              );
            }
        ),
        floatingActionButton: FloatingActionButton.extended(
          label: const Text('Start Stream'),
          hoverColor: Colors.green,
          onPressed: () {
            print('Sending messages...');
            // fetchImage();
            sendImage();
          },
        ),
      ),
    );
  }

  sendImage() async {
    imageStream.getImage().listen((eventImage) {
      setState(() {
        //print(eventVocab);
        images.add(eventImage);
        print(eventImage); // somehow images does not load...!?
      });
    });
  }

}