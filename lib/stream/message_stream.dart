class MessageStream {
  // Stream colorStream;

  Stream<String> getMessage() async* {
    final List<String> messages = [
      'Hello!?',
      'How are you?',
      'Welcome to my message stream.',
      'The weather is good today...',
      'It\'s getting cold.',
      'That\'s it for today :)',
      'Bye.'
    ];

    yield* Stream.periodic(const Duration(seconds: 1), (int t) {
      int index = t % messages.length;
      return messages[index];
    });
  }
}