import 'dart:convert';
import 'package:http/http.dart' as http;

class ImagesStream {
  // Stream colorStream;

  Stream<String> getImage() async* {
    final List<String> images = [];

    const String urlString = 'https://jsonplaceholder.typicode.com/photos';
    var response = await http.read(Uri.parse(urlString));
    var jsonObject = json.decode(response);

    for (int i = 0; i < jsonObject.length; i++) {
      images.add(jsonObject[i]['url']);
    }

    // fetchImage() async {
    //   const String urlString = 'https://jsonplaceholder.typicode.com/photos';
    //   var response = await http.read(Uri.parse(urlString));
    //   var jsonObject = json.decode(response);
    // }

    yield* Stream.periodic(const Duration(seconds: 1), (int t) {
      int index = t % images.length;
      return images[index];
    });
  }
}